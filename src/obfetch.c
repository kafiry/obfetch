#include <sys/utsname.h>
#include <sys/time.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <sys/statvfs.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util.h>
#include <err.h>

#include "color.h"

#define COUNT(x) (int)(sizeof x / sizeof *x)
#define MAX(x,y) ((x) >= (y)?(x):(y) )
#define MIN(x,y) ((x) < (y)?(x):(y) )

#define MAX_SIZE  255

const unsigned int OFFSET = strlen(RST) + strlen(YEL);
/*
 *
 */
char os_str[MAX_SIZE] = "";
char host_str[MAX_SIZE] = "";
char cpu_str[MAX_SIZE] = "";
char uptime_str[MAX_SIZE] = "";
char shell_str[MAX_SIZE] = "";
char memory_str[MAX_SIZE] = "";
char pkg_str[MAX_SIZE] = "";
char disk_str[MAX_SIZE] = "";

char normal_col[] = "\x1b[40m  \x1b[41m  \x1b[42m  \x1b[43m  \x1b[44m  \x1b[45m  \x1b[46m  \x1b[47m  " RST;
char bright_col[] = "\x1b[100m  \x1b[101m  \x1b[102m  \x1b[103m  \x1b[104m  \x1b[105m  \x1b[106m  \x1b[107m  " RST;

#define INFO_LEN 10

char *info_name[INFO_LEN] = {BYEL "OS:" RST, BYEL "Host:" RST, BYEL "CPU:" RST, BYEL "Uptime:" RST, BYEL "Shell:" RST, BYEL "Memory:" RST, BYEL "Pkgs:" RST, BYEL "Home_Disk:" RST, "normal_col", "bright_col"};
/*
 *
 */
char *info_data[INFO_LEN] = {
	os_str,
	host_str,
	cpu_str,
	uptime_str,
	shell_str,
	memory_str,
	pkg_str,
	disk_str,
	normal_col,
	bright_col
};
/*
 *
 */
char *
add_space(char *space, unsigned int len)
{
	for (unsigned int i = 0; i < len; i++) {
		strlcat(space, " ", MAX_SIZE);
	}
	return space;
}
/*
 * x -> LOGO[] size
 * y -> size of info_name[] == info_data[]
 */
static void
print_info(char *logo[], char *info_name[], char *info_data[], unsigned short int x, unsigned short int y)
{
	unsigned short int i = 0;
	char spaces[MAX_SIZE] = "";
	unsigned short int a, b;

	a = MAX(x, y);
	b = MIN(x, y);
	add_space(spaces, (unsigned int) (strlen(*logo) - OFFSET));

	for (i = 0; i < a; i++) {
		if (i < b) {
			if (!(strncmp(info_name[i], "normal_col", MAX_SIZE) && strncmp("bright_col", info_name[i], MAX_SIZE)))
				printf("%s  %s\n", logo[i], info_data[i]);
			else
			printf("%s  %s %s\n", logo[i], info_name[i], info_data[i]);
		} else {
			if (x >= y) {
				printf("%s\n", logo[i]);
			} else {
				if (!(strncmp(info_name[i], "normal_col", MAX_SIZE) && strncmp("bright_col", info_name[i], MAX_SIZE)))
					printf("%s  %s\n", spaces, info_data[i]);
				else
				printf("%s  %s %s\n", spaces, info_name[i], info_data[i]);
			}
		}
	}
}
/*
 *
 */
static void
disk_info(void)
{
	const unsigned int GB = (1024 * 1024) * 1024;
	struct statvfs info;

	if (!statvfs(getenv("HOME"), &info)) {
		unsigned long disk_left = (info.f_bfree * info.f_frsize);
		unsigned long disk_total = (info.f_blocks * info.f_frsize);
		unsigned long disk_used = disk_total - disk_left;
		float disk_perc = ((float) disk_used / (float) disk_total * 100);

		snprintf(disk_str, MAX_SIZE, "%.1fG / %.1fG[%.1f%%]", ((float) (disk_used)) / GB, ((float) (disk_total)) / GB, disk_perc);
	}
	return;
}
/*
 * man 3 uname
*/
static void
sys_info()
{
	struct utsname uts;

	if (0 > uname(&uts)) {
		//fprintf(stderr, "uname error!\n");
		//exit(1);
		err(1, "uname error!");
	} else {
		snprintf(os_str, MAX_SIZE, "%s %s %s", uts.sysname, uts.release, uts.machine);
	}
	return;
}
/*
 * sysctl - n hw.version
 * int sysctl(const int *name, u_int namelen, void *oldp, size_t *oldlenp, void *newp, size_t newlen);
 */
static void
host_info()
{
	int mib[] = {CTL_HW, HW_VERSION};
	size_t len;
	char *pv;

	if (sysctl(mib, 2, NULL, &len, NULL, 0) == -1)
		err(1, "sysctl");
	if (NULL == (pv = malloc(len)))
		err(1, "sysctl hw.version malloc error!");
	if (sysctl(mib, 2, pv, &len, NULL, 0) == -1)
		err(1, "sysctl hw.version error!");
	//printf("hw vwesion: %s\n", pv);
	snprintf(host_str, MAX_SIZE, "%s", pv);
	free(pv);
	return;
}
/*
 * hw.model
 */
static void
cpu_info()
{
	int mib[] = {CTL_HW, HW_MODEL};
	size_t len;
	char *pc;

	if (sysctl(mib, 2, NULL, &len, NULL, 0) == -1)
		err(1, "sysctl");
	if (NULL == (pc = malloc(len)))
		err(1, "malloc");
	if (sysctl(mib, 2, pc, &len, NULL, 0) == -1)
		err(1, "sysctl hw.model error!");

	snprintf(cpu_str, MAX_SIZE, "%s", pc);
	free(pc);

	return;
}
/*
 * man clock_gettime
 */
static void
uptime_info()
{
	struct timespec uptime;
	unsigned int days;
	unsigned short int hrs, min;

	if (clock_gettime(CLOCK_BOOTTIME, &uptime) < 0) {
		err(1, "error");
	}
	days = uptime.tv_sec / 86400;
	hrs = uptime.tv_sec % 86400 / 3600;
	min = uptime.tv_sec % 3600 / 60;


	if (days > 0) {
		snprintf(uptime_str, MAX_SIZE, "%dd %dh %dm", days, hrs, min);
	} else {
		snprintf(uptime_str, MAX_SIZE, "%dh %dm", hrs, min);
	}
	return;
}
/*
 *
 */
static void
shell_info()
{
	char *shellInfo;

	if (NULL != (shellInfo = getenv("SHELL"))) {
		snprintf(shell_str, MAX_SIZE, "%s", shellInfo);
	} else {
		//fprintf(stderr, "getenv error!\n");
		err(1, "getenv error!");
		//exit(1);
	}
	return;
}
/*
 *
 */
static void
memory_info()
{
	int mib[] = {CTL_VM, VM_UVMEXP};
	struct uvmexp uvmexp_st;
	size_t len;
	long long active_mem, total_mem;
	char active_buf[FMT_SCALED_STRSIZE];
	char total_buf[FMT_SCALED_STRSIZE];

	len = sizeof(uvmexp_st);

	if (sysctl(mib, 2, &uvmexp_st, &len, NULL, 0) == -1)
		err(1, "sysctl uvmexp error!");

	total_mem = (size_t) (uvmexp_st.npages << (uvmexp_st.pageshift - 10)) * 1024;
	active_mem = (size_t) (uvmexp_st.active << (uvmexp_st.pageshift - 10)) * 1024;

	if (fmt_scaled(active_mem, active_buf) == 0 && fmt_scaled(total_mem, total_buf) == 0)
		snprintf(memory_str, MAX_SIZE, "%s / %s", active_buf, total_buf);
	else
		err(1, "fmt scaled failed");

	return;
}
/*
 * OpenBSD package
 */
static void
pkg_info(void)
{
	FILE *fpt;
	int packages = 0;

	fpt = popen("/bin/ls -h /var/db/pkg | wc -l | xargs", "r");
	fscanf(fpt, "%d", &packages);
	pclose(fpt);

	snprintf(pkg_str, MAX_SIZE, "%d", packages);
	return;
}
/*
 * openbsd ascii from ufetch
 */
char *openbsd_mini_logo[] = {
	//BYEL "                " RST,
	BYEL "      _____     " RST,
	BYEL "    \\-     -/   " RST,
	BYEL " \\_/         \\  " RST,
	BYEL " |        " RST "O O " BYEL "| " RST,
	BYEL " |_  <   )  3 ) " RST,
	BYEL " / \\         /  " RST,
	BYEL "    /-_____-\\   " RST
};

char *openbsd_logo[] = {
	"                                       " BCYN " _  " RST "",
	"                                       " BCYN "(_) " RST "",
	BYEL "              |    .                       " RST "",
	BYEL "          .   |L  /|   .           " BCYN "   _    " RST,
	BYEL "      _ . |\\ _| \\--+._/| .           " BCYN "(_)   " RST,
	BYEL "     / ||\\| Y J  )   / |/| ./              " RST,
	BYEL "    J  |)'( |        ` F`.'/   " BCYN "     _      " RST,
	BYEL "  -<|  F         __     .-<        " BCYN "(_)     " RST,
	BYEL "    | /       .-'" BCYN ". " BYEL "`.  /" BCYN "-. " BYEL "L___            " RST,
	BYEL "    J \\      <    " BCYN "\\ " BYEL " | | " BBLK "O" BCYN "\\" BYEL "|.-' " BCYN
	"  _        " RST,
	BYEL "  _J \\  .-    \\" BCYN "/ " BBLK "O " BCYN "| " BYEL "| \\   |" BYEL "F    " BCYN
	"(_)       " RST,
	BYEL " '-F  -<_.     \\   .-'  `-' L__            " RST,
	BYEL "__J  _   _.     >-'  " YEL ")" BRED "._.   " BYEL "|-'            " RST,
	BYEL " `-|.'   /_.          " BRED "\\_|  " BYEL " F              " RST,
	BYEL "  /.-   .                _.<               " RST,
	BYEL " /'    /.'             .'  `\\              " RST,
	BYEL "  /L  /'   |/      _.-'-\\                  " RST,
	BYEL " /'J       ___.---'\\|                     " RST,
	BYEL "   |\\  .--' V  | `. `                     " RST,
	BYEL "   |/`. `-.     `._)                      " RST,
	BYEL "      / .-.\\                            " RST,
	BYEL "      \\ (  `\\                           " RST "",
	BYEL "       `.\\                                  " RST ""
};
/*
 *
 */
int
main()
{
	sys_info();
	disk_info();
	pkg_info();
	shell_info();
	host_info();
	uptime_info();
	memory_info();
	cpu_info();

	print_info(openbsd_mini_logo, info_name, info_data, COUNT(openbsd_mini_logo), INFO_LEN);
	//print_info(openbsd_logo, info_name, info_data, COUNT(openbsd_logo), INFO_LEN);

	return 0;
}
