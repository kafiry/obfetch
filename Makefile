CC ?= cc
CFLAGS += -O2 -std=c99 -Wall -Wextra -Werror
LDFLAGS += -lutil
#PREFIX ?= /usr/local
PREFIX ?= ~/.local/share


all: obfetch

obfetch: src/obfetch.c src/color.h
	${CC} ${CFLAGS} src/obfetch.c ${LDFLAGS} -o obfetch

install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp obfetch ${DESTDIR}${PREFIX}/bin
	chmod 711 ${DESTDIR}${PREFIX}/bin/obfetch

clean:
	rm -rf obfetch

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/obfetch

.PHONY: all clean install uninstall
